# Gaze estimation - Control the pc cursor with your eyes

*A team project made for the course 'Computer Vision' at the Hebrew University.* 

![](pupil_estimation.png)

This project aims at controlling the cursor of a low end personal computer using eye tracking.

## The process involved:
*	Getting face features by using the 'One Millisecond Face Alignment with an Ensemble of Regression Trees' algorithm.
*	Estimating the head position by solving the Perspective-n-Point problem. 
*	Locating the pupil center using the Circle Hough transform on the edges of blurred extracted eye images.
*	Estimating gaze ray using a 3D model of the human head and eye
*	Locating where the gaze intersects with the screen
*	Controlling the cursor using above information.

Further details can be found in the accompanying [presentation](https://gitlab.com/Hanan_Levin/gaze-estimation/-/blob/master/Face_features_and_eye_gaze.pptx)


	

