import cv2
import numpy as np
import dlib
import time
from collections import deque
import pyautogui

# location of face part in the 68 point face array
RIGHT_EYE = [i for i in range(42, 48)]
LEFT_EYE = [i for i in range(36, 42)]
MOUTH = [i for i in range(60, 68)]

# thresholds for detecting closing of the eye and opening of the mouth
EYE_AR_INITIAL_THRESH = 0.3  # initial thresh to put in the queue later
MOUTH_MAR_THRESH = 0.4

# Nose, Chin, Left eye corners, Right eye corners, Mouth corners, from the face array
SIX_POINT_FACE_REPRESENTATION = [30, 8, 36, 45, 48, 54]

# Corresponding 3D proportions
HEAD_3D_POINTS = np.array([
    (0.0, 0.0, 0.0),  # Nose tip
    (0.0, -330.0, -65.0),  # Chin
    (-225.0, 170.0, -135.0),  # Left eye left corner
    (225.0, 170.0, -135.0),  # Right eye right corner
    (-150.0, -150.0, -125.0),  # Left Mouth corner
    (150.0, -150.0, -125.0)  # Right mouth corner
])
PROPORTION_TO_CENTIMETER = 75  # constant to go from world proportions to cm

CENTER_RIGHT_EYE_BALL = (175.5, 177.5, -230)
CENTER_LEFT_EYE_BALL = (175.5, 177.5, -230)
RADIUS_EYE_BALL = 1.25 * PROPORTION_TO_CENTIMETER  # 1.25 centimeter(scientific) times the scaling factor per centimeter we used.

EYE_ANLGE_OF_VIEW = 150  # the eye lid is open 150 degrees.
CORNEA_ANGLE = 30  # roughly measured by the fact that the radius of the cornea is approx. 6.5 mm

PIXELS_PER_CENTIMETER = 37.8

# the measured distace of the screen from my laptop camera - 1.1 centimeters, times PIXELS_PER_CENTIMETER
DISTANCE_FROM_CAMERA_TO_SCREEN_IN_PIXELS = 42

SINGLE_EYE_IMG_SIZE = (28, 28)  # for display

WEIGHTED_AVERAGE_ALPHA = 0.1


def estimate_gaze_point_on_screen(camera_properties, referece_point_3D_1, reference_3D_point_2, move_mouse=True):
    """
    Given 2 reference points on a line in the world coordinates, find the location of intersection of that line
    with the plane of the screen in the camera coordinates and move the mouse to that location.
    If move_mouse is true -> automatically move the mouse to the estimated position
    """

    rotation_vector, translation_vector, calibration_matrix, dist_coeffs = camera_properties

    # get the projection matrtix from the nose coordinates to the camera coordinates using the Rodrigues method
    empty_rotation_matix = np.zeros((3, 3))
    rotation_matix = cv2.Rodrigues(rotation_vector, empty_rotation_matix)[0]
    projectioin_matrix = np.column_stack((rotation_matix, translation_vector))

    # get 2 points on the  line in camera coordinates
    p1 = projectioin_matrix @ np.concatenate((referece_point_3D_1, [1]), axis=0)  # using homogeneous coordinates
    p2 = projectioin_matrix @ np.concatenate((reference_3D_point_2, [1]), axis=0)

    # line equation is p1 + (p2-p1)*t.  Find t for z=0, which will give us the intersection with the plane of the camera
    t = -1 * (p1[2] / (p2[2] - p1[2]))
    intersection_point = p1 + (p2 - p1) * t

    # change to position in pixels
    intersection_point_centi = intersection_point / PROPORTION_TO_CENTIMETER  # change units to centimeters
    intersection_point_pixels = intersection_point_centi * PIXELS_PER_CENTIMETER


    # find position on the screen
    width, height = pyautogui.size()
    pos_x = intersection_point_pixels[0] + width / 2
    pos_y = intersection_point_pixels[1] + height / 2
    pos_y = pos_y - DISTANCE_FROM_CAMERA_TO_SCREEN_IN_PIXELS
    if 0 < pos_x < width and 0 < pos_y < height:
        mouse_location_queue.append(np.array([pos_x, pos_y]))

        weighted_location = WEIGHTED_AVERAGE_ALPHA * mouse_location_queue[1] + \
                            (1 - WEIGHTED_AVERAGE_ALPHA) * mouse_location_queue[0]
        if move_mouse:
            pyautogui.moveTo(int(weighted_location[0]), int(weighted_location[1]))
        return weighted_location


def find_pupil_center_in_3D(img, face_shape_coords, pupil_center_2D, camera_properties):
    """
    Calculate the 3D position of the pupil, knowing its 2D position in the image, and the camera matrix
    Use the simple assumption that each movement in pixels corresponds to a uniform movement in the angle from the
    center of the eye.
    """
    right_eye_corners = [face_shape_coords[42], face_shape_coords[45]]
    right_eye_pixel_length = np.linalg.norm(right_eye_corners[0] - right_eye_corners[1])
    degrees_per_pixel = ((EYE_ANLGE_OF_VIEW / right_eye_pixel_length) * np.pi) / 180

    center_of_eye_corners = np.mean(right_eye_corners, axis=0)

    center_of_corners_tup = (int(center_of_eye_corners[0]), int(center_of_eye_corners[1]))
    cv2.circle(img, center_of_corners_tup, 2, (255, 100, 200))

    vector_pupil_from_corner_center = pupil_center_2D - center_of_eye_corners
    theta = vector_pupil_from_corner_center[0] * degrees_per_pixel
    phi = -1 * vector_pupil_from_corner_center[1] * degrees_per_pixel

    vector_pupil_from_eye_center = np.array([RADIUS_EYE_BALL * np.sin(theta) * np.cos(phi),
                                             RADIUS_EYE_BALL * np.sin(theta) * np.sin(phi),
                                             RADIUS_EYE_BALL * np.cos(theta)])

    pupil_center_3D = CENTER_RIGHT_EYE_BALL + vector_pupil_from_eye_center

    draw_pupil_visualization_lines(pupil_center_3D, center_of_corners_tup, theta, phi)

    return pupil_center_3D


def draw_pupil_visualization_lines(pupil_center_3D, center_of_corners_tup, theta, phi):
    """
    draw the projection of the 3D pupil center, lines from the center of the eye balls to the corners, and a line
    passing through the middle of the eye ball and the pupil cetner.
    """
    rotation_vector, translation_vector, calibration_matrix, dist_coeffs = camera_properties
    (pupil_center_projection, _) = cv2.projectPoints(np.array([pupil_center_3D]), rotation_vector,
                                                     translation_vector,
                                                     calibration_matrix, dist_coeffs)

    # darw projection of the calculated 3D pupil center
    pupil_center_projection_tuple = (int(pupil_center_projection[0][0][0]), int(pupil_center_projection[0][0][1]))
    cv2.circle(img, pupil_center_projection_tuple, 1, (255, 10, 255), thickness=7)

    # line from center of corners to pupil center
    pupil_center_tup = (int(pupil_center_2D[0]), int(pupil_center_2D[1]))
    cv2.line(img, center_of_corners_tup, pupil_center_tup, (0, 0, 0), 2)

    # draw the lines from the center of the right eye ball to corner of the eye for sanity.
    ############################################################################################
    theta_right_corner = ((-EYE_ANLGE_OF_VIEW / 2) * np.pi) / 180
    phi_right_corner = 0
    vector_right_corner_from_eye_center = np.array(
        [RADIUS_EYE_BALL * np.sin(theta_right_corner) * np.cos(phi_right_corner),
         RADIUS_EYE_BALL * np.sin(theta_right_corner) * np.sin(phi_right_corner),
         RADIUS_EYE_BALL * np.cos(theta_right_corner)])
    right_corner_3D = CENTER_RIGHT_EYE_BALL + vector_right_corner_from_eye_center
    (right_corner_projection, _) = cv2.projectPoints(np.array([right_corner_3D]), rotation_vector,
                                                     translation_vector,
                                                     calibration_matrix, dist_coeffs)

    right_corner_tup = (int(right_corner_projection[0][0][0]), int(right_corner_projection[0][0][1]))
    (right_eyeball_center, _) = cv2.projectPoints(np.array([CENTER_RIGHT_EYE_BALL]), rotation_vector,
                                                  translation_vector,
                                                  calibration_matrix, dist_coeffs)
    right_center = (int(right_eyeball_center[0][0][0]), int(right_eyeball_center[0][0][1]))

    cv2.line(img, right_center, right_corner_tup, (120, 255, 80), 2)

    ############################################################################################
    theta_left_corner = ((EYE_ANLGE_OF_VIEW / 2) * np.pi) / 180
    phi_left_corner = 0
    vector_left_corner_from_eye_center = np.array(
        [RADIUS_EYE_BALL * np.sin(theta_left_corner) * np.cos(phi_left_corner),
         RADIUS_EYE_BALL * np.sin(theta_left_corner) * np.sin(phi_left_corner),
         RADIUS_EYE_BALL * np.cos(theta_left_corner)])
    left_corner_3D = CENTER_RIGHT_EYE_BALL + vector_left_corner_from_eye_center
    (left_corner_projection, _) = cv2.projectPoints(np.array([left_corner_3D]), rotation_vector,
                                                    translation_vector,
                                                    calibration_matrix, dist_coeffs)

    left_corner_tup = (int(left_corner_projection[0][0][0]), int(left_corner_projection[0][0][1]))

    cv2.line(img, right_center, left_corner_tup, (180, 50, 20), 2)

    ######################
    # draw x ray lines from middle of eye ball passing through the pupil center
    radius_multiplier = 20
    long_vector_pupil_from_eye_center = np.array(
        [RADIUS_EYE_BALL * radius_multiplier * np.sin(theta) * np.cos(phi),
         RADIUS_EYE_BALL * radius_multiplier * np.sin(theta) * np.sin(phi),
         RADIUS_EYE_BALL * radius_multiplier * np.cos(theta)])

    x_ray_line_end_3D = CENTER_RIGHT_EYE_BALL + long_vector_pupil_from_eye_center

    rotation_vector, translation_vector, calibration_matrix, dist_coeffs = camera_properties
    (projection_of_x_ray_line, _) = cv2.projectPoints(np.array([x_ray_line_end_3D]), rotation_vector,
                                                      translation_vector,
                                                      calibration_matrix, dist_coeffs)

    projection_of_x_ray_line_tup = (int(projection_of_x_ray_line[0][0][0]), int(projection_of_x_ray_line[0][0][1]))
    cv2.line(img, right_center, projection_of_x_ray_line_tup, (0, 0, 255), 1)


def estimate_head_pos(img, face_shape_coords):
    """
    Estimate the direction the head is facing by finding the transformation matrix from the world(nose) coordinates
    to the camera coordinates.
    """
    image_plane_points = face_shape_coords[SIX_POINT_FACE_REPRESENTATION].astype('double')
    size = img.shape

    # Camera internals
    focal_length = size[1]  # width of the image as an approximation
    center = (size[1] / 2, size[0] / 2)  # optical center as  the center of the image - an approximation
    calibration_matrix = np.array(
        [[focal_length, 0, center[0]],
         [0, focal_length, center[1]],
         [0, 0, 1]], dtype="double"
    )
    dist_coeffs = np.zeros((4, 1))  # Assuming no lens distortion

    # solve the PnP problem - get the camera matrix
    (success, rotation_vector, translation_vector) = cv2.solvePnP(HEAD_3D_POINTS, image_plane_points,
                                                                  calibration_matrix,
                                                                  dist_coeffs)

    # project lines of head direction and lines from center of eyes to both eye corners.
    (nose_end_point2D, _) = cv2.projectPoints(np.array([(0.0, 0.0, 400.0)]), rotation_vector, translation_vector,
                                              calibration_matrix, dist_coeffs)
    (right_eyeball_center, _) = cv2.projectPoints(np.array([CENTER_RIGHT_EYE_BALL]), rotation_vector,
                                                  translation_vector,
                                                  calibration_matrix, dist_coeffs)
    (left_eyeball_center, _) = cv2.projectPoints(np.array([CENTER_LEFT_EYE_BALL]), rotation_vector,
                                                 translation_vector,
                                                 calibration_matrix, dist_coeffs)

    p1 = (int(image_plane_points[0][0]), int(image_plane_points[0][1]))
    p2 = (int(nose_end_point2D[0][0][0]), int(nose_end_point2D[0][0][1]))
    p3 = (int(right_eyeball_center[0][0][0]), int(right_eyeball_center[0][0][1]))
    p4 = (int(left_eyeball_center[0][0][0]), int(left_eyeball_center[0][0][1]))

    cv2.line(img, p1, p2, (0, 0, 0), 2)
    cv2.line(img, p1, p3, (255, 255, 255), 2)  # nose to right eye
    cv2.line(img, p1, p4, (255, 255, 255), 2)  # nose to left eye

    return rotation_vector, translation_vector, calibration_matrix, dist_coeffs


def is_blinking(eye_shape_array, eye):
    """
    eye is 0 for right, 1 for left
    Based on the work by Soukupová and Čech in their 2016 paper, Real-Time Eye Blink Detection using Facial Landmarks,
    we can then derive an equation that reflects this relation called the eye aspect ratio (EAR)
    :param eye_shape_array: 6 points
    :return: True of closed
    """
    p1, p2, p3, p4, p5, p6 = eye_shape_array

    nominator = np.linalg.norm(p2 - p6) + np.linalg.norm(p3 - p5)
    denominator = 2 * np.linalg.norm(p1 - p4)
    EAR = nominator / denominator

    if not eyes_closed[eye]:
        eyes_averages[eye].append(EAR)
    EAR_average = np.mean(list(eyes_averages[eye]))
    EAR_min = np.min(list(eyes_averages[eye]))
    blink_threshold = EAR_min + (EAR_average - EAR_min) / 2
    if EAR < blink_threshold:
        return True
    return False


def is_mouth_open(mouth_array):
    """
    Based on the work by Soukupová and Čech in their 2016 paper, Real-Time Eye Blink Detection using Facial Landmarks,
    we can then derive an equation that reflects this relation called the eye aspect ratio (EAR).
    Used this time on mouth, to get a similar result. Recogniseze when mouth is opened.
    :param eye_shape_array: 6 points of the mouth
    :return:
    """
    p1, p2, p3, p4, p5, p6, p7, p8 = mouth_array

    nominator = np.linalg.norm(p2 - p8) + np.linalg.norm(p4 - p6)
    denominator = 2 * np.linalg.norm(p1 - p5)

    MAR = nominator / denominator  # Mouth aspect ration

    if MAR > MOUTH_MAR_THRESH:
        return True
    return False


def count_frames(start_frame_time, frame_count):
    """
    Used to count frame rate.
    """
    frame_count += 1
    if time.time() - start_frame_time > 1:
        # print(f"{frame_count} frames per second")
        frame_count = 0
        start_frame_time = time.time()
    return start_frame_time, frame_count


def estimate_pupil(img, face_shape_coords):
    """"
    Estimate center of pupil 2D location using Hough circles
    display an image of the estimated pupil
    """
    (x, y, w, h) = cv2.boundingRect(face_shape_coords[RIGHT_EYE])
    right_eye = img[max(0, int(y - h / 2)): int(y + h * 1.5), x:x + w]
    for (i, eye_image) in enumerate([right_eye]):

        # pre process by a gaussian kernel and canny edge detection
        gray_img = cv2.cvtColor(eye_image, cv2.COLOR_BGR2GRAY)
        # Blur the image to reduce noise
        blur = cv2.medianBlur(gray_img, 5)
        # Canny edges
        edges = cv2.Canny(blur, 120, 3)

        # Calculate the expected cornea size based on the 3D world.
        right_eye_corners = [face_shape_coords[42], face_shape_coords[45]]
        right_eye_pixel_length = np.linalg.norm(right_eye_corners[0] - right_eye_corners[1])
        pixels_per_degree = right_eye_pixel_length / EYE_ANLGE_OF_VIEW

        radius_pixel_estimation = CORNEA_ANGLE * pixels_per_degree
        rounded_estimation = int(round(radius_pixel_estimation))

        # multiply the found estimations to allow for some variance
        rounded_min = int(0.8 * rounded_estimation)
        rounded_max = int(np.ceil(1.3 * rounded_estimation))

        # Apply hough transform on the image with the found min and max radius
        circles = cv2.HoughCircles(edges, cv2.HOUGH_GRADIENT, 1, 100, param1=200, param2=10, minRadius=rounded_min,
                                   maxRadius=rounded_max)

        if circles is not None:
            circles = np.uint16(np.around(circles))
            for j in circles[0, :]:
                cv2.circle(eye_image, (j[0], j[1]), j[2], (0, 120, 255), 1)
                cv2.circle(img, (x + j[0], int(y - h / 2) + j[1]), 1, (120, 120, 10), 1)

                # show eye image
                if i == 0:
                    resized = cv2.resize(eye_image, (78, 78))
                    cv2.imshow("RIGHT EYE", resized)
                else:
                    resized = cv2.resize(eye_image, (78, 78))
                    cv2.imshow("LEFT EYE", resized)
                cv2.waitKey(1)
                return np.array([x + j[0], int(y - h / 2) + j[1]])


def detect_mouth_open(mouth_open, face_shape_coords):
    """
    detect if the mouth has been opened
    :return: True if mouth has been opened
    """
    opened_mouth = is_mouth_open(face_shape_coords[MOUTH])
    if opened_mouth:
        if not mouth_open:
            print("opened mouth")
            mouth_open = True
    else:
        mouth_open = False
    return mouth_open


def draw_face_points(face_shape_coords):
    """
    Draw face points, with special colors for eyes and mouths
    :param face_shape_coords:
    :return:
    """
    for (x, y) in face_shape_coords:
        cv2.circle(img, (x, y), 1, (255, 255, 0), 1)
    for (x, y) in face_shape_coords[RIGHT_EYE]:
        cv2.circle(img, (x, y), 1, (0, 0, 255), 1)
    for (x, y) in face_shape_coords[LEFT_EYE]:
        cv2.circle(img, (x, y), 1, (0, 255, 0), 1)
    for (x, y) in face_shape_coords[MOUTH]:
        cv2.circle(img, (x, y), 1, (0, 255, 255), 1)


if __name__ == '__main__':
    # initialize detectors
    face_detector = dlib.get_frontal_face_detector()

    # download here: https://github.com/AKSHAYUBHAT/TensorFace/blob/master/openface/models/dlib/   ->
    # -> shape_predictor_68_face_landmarks.dat
    predictor = dlib.shape_predictor("shape_predictor_68_face_landmarks.dat")

    video = cv2.VideoCapture(0)

    frame_count = 0
    start_frame_time = time.time()

    # Windows to average over for better detections
    right_eye_EAR_average = deque([EYE_AR_INITIAL_THRESH], maxlen=50)
    left_eye_EAR_average = deque([EYE_AR_INITIAL_THRESH], maxlen=50)
    eyes_averages = [right_eye_EAR_average, left_eye_EAR_average]
    eye_edges_list = deque([EYE_AR_INITIAL_THRESH], maxlen=4)
    mouse_location_queue = deque([np.array([0, 0])], maxlen=2)  # to do a weighted average on

    right_eye_closed = False
    left_eye_closed = False
    mouth_open = False
    eyes_closed = [right_eye_closed, left_eye_closed]

    # apply detections on video
    while True:
        check, img = video.read()
        img = cv2.flip(img, 1)  # mirror
        gray = cv2.cvtColor(img, cv2.COLOR_BGR2GRAY)
        face_rects = face_detector(gray, 1)

        for rect in face_rects:
            shape = predictor(gray, rect)
            face_shape_coords = np.zeros((68, 2), dtype=int)

            # convert to np arrays
            for i in range(0, 68):
                face_shape_coords[i] = np.array([shape.part(i).x, shape.part(i).y])

            # convert to openCV bounding boxes
            x, y = rect.left(), rect.top()
            w, h = rect.right() - x, rect.bottom() - y

            cv2.rectangle(img, (x, y), (x + w, y + h), (255, 255, 255), 3)
            pupil_center_2D = estimate_pupil(img, face_shape_coords)
            camera_properties = estimate_head_pos(img, face_shape_coords)
            if pupil_center_2D is not None:
                pupil_center_3D = find_pupil_center_in_3D(img, face_shape_coords, pupil_center_2D, camera_properties)
                estimate_gaze_point_on_screen(camera_properties, CENTER_RIGHT_EYE_BALL, pupil_center_3D)

            draw_face_points(face_shape_coords)

            mouth_open = detect_mouth_open(mouth_open, face_shape_coords)
            blink = is_blinking(face_shape_coords[RIGHT_EYE], 0)

        cv2.imshow("mirror", img)
        key = cv2.waitKey(1)
        if key == ord("q"):
            break
        start_frame_time, frame_count = count_frames(start_frame_time, frame_count)

    cv2.destroyAllWindows()
    video.release()
